select o.parent_id bundle_product_id, s.option_id, o.required is_required, (
    select if(count(*) > 0, 'yes', 'no')
    from catalog_product_bundle_selection s2
    where s2.option_id = option_id
    and s2.parent_product_id = s.parent_product_id
    and is_default = 1
) has_default, count(selection_id) num_of_bundle_options
from catalog_product_bundle_option o
inner join catalog_product_bundle_selection s
on s.parent_product_id = o.parent_id
where o.required = 1
and o.type = 'radio'
group by s.option_id
having has_default = 'no'
order by bundle_product_id, s.option_id;