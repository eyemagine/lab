select
    e.event_id EventId,
    concat(e.entity, '::', e.type, '(), id: ', e.entity_pk) EventTrigger,
    pe.status EventProcessStatus,
    p.indexer_code IndexProcessTriggered,
    e.created_at TimeOfEvent,
    p.ended_at LastReindex
from mage_index_event e
inner join mage_index_process_event pe
    on pe.event_id = e.event_id
inner join mage_index_process p
    on p.process_id = pe.process_id
order by TimeOfEvent DESC