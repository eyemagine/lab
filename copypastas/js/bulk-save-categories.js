/**
 * Paste each chunk of code one at a time into the chrome console while on the
 * category page in the admin.
 *
 * Follow instructions in comments where present
 */

// Paste 1
var script = document.createElement('script');
script.src = 'http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);

// Paste 2
jQuery.noConflict();

// use inspector to add "thisOne" id to desired surrounding ul in cat list
// Paste 3
// do this a couple of times until all cats are expanded
jQuery('#thisOne .x-tree-elbow-plus').click()

// Paste 4
var cats = jQuery("#thisOne").find(".x-tree-node-el.active-category a");

function openCat(i) {
    if (jQuery("#loading-mask:visible").length) {
        setTimeout(function() {
            openCat(i)
        }, 1000);
    }
    else {
        cats[i].click();
        console.log("open " + jQuery(cats[i]).text());
        saveCat(i);
    }
}

function saveCat(i) {
    if (jQuery("#loading-mask:visible").length) {
        setTimeout(function() {
            saveCat(i)
        }, 1000);
    }
    else {
        jQuery("button[title=Save Category]").click();
        console.log("save " + i + "/" + cats.length);
        if (i < cats.length - 1) openCat(++i)
    }
}

openCat(0);