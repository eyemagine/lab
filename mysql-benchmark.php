<?php

ini_set("display_errors", 1);
include 'thebooknook/db.php';

function plural($count, $plural = 's', $singular = '') {
    if (is_bool($plural) && $plural) return $count == 1;
    return $count == 1 ? $singular : $plural;
}

function ordinalSuffix($num){
    if($num < 11 || $num > 13){
         switch($num % 10){
            case 1: return $num . 'st';
            case 2: return $num . 'nd';
            case 3: return $num . 'rd';
        }
    }
    return $num . 'th';
}

try {
    $db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch (PDOException $e) {
    print "SQL Error: " . $e->getMessage() . "<br/>";
    die();
}

$queries = array(
'select
    s.name `Store View`,
    date_format(u.visit_time, "%Y-%m-%d") Period,
    count(distinct vi.remote_addr) `Unique Visits`,
    count(*) Visits,
    replace(url, concat(substring_index(url, "/", 3), "/"), "") Page
from log_url u
inner join log_visitor_info vi
    on vi.visitor_id = u.visitor_id
inner join log_url_info i
    on i.url_id = u.url_id
inner join log_visitor v
    on v.visitor_id = vi.visitor_id
inner join core_store s
    on s.store_id = v.store_id
where u.visit_time > now() - interval 1 month
group by Period, v.store_id, Page
order by Period DESC, Visits DESC, `Store View`',
'select
    s.name `Store View`,
    date_format(u.visit_time, "%Y-%m-%d") Period,
    count(distinct vi.remote_addr) `Unique Visits`,
    replace(url, concat(substring_index(url, "/", 3), "/"), "") Page
from log_url u
inner join log_visitor_info vi
    on vi.visitor_id = u.visitor_id
inner join log_url_info i
    on i.url_id = u.url_id
inner join log_visitor v
    on v.visitor_id = vi.visitor_id
inner join core_store s
    on s.store_id = v.store_id
where u.visit_time > now() - interval 1 month
group by Period, v.store_id, Page
order by Period DESC, `Unique Visits` DESC, `Store View`',
'select
    date_format(u.visit_time, "%Y-%m-%d") Period,
    count(distinct vi.remote_addr) `Unique Visits`,
    count(*) Visits,
    replace(url, concat(substring_index(url, "/", 3), "/"), "") Page
from log_url u
inner join log_visitor_info vi
    on vi.visitor_id = u.visitor_id
inner join log_url_info i
    on i.url_id = u.url_id
inner join log_visitor v
    on v.visitor_id = vi.visitor_id
where u.visit_time > now() - interval 1 month
group by Period, Page
order by Period DESC, Visits DESC'
);

$times = 100;
?><html>
    <head>
        <title>MySQL Benchmark</title>
        <style type="text/css">

            body {
                font-family: Arial;
            }

            hr {
                margin-top: 20px;
            }

            table {
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                text-align: left;
                padding: 5px;
            }

            tbody th {
                text-transform: right;
            }

            td {
                border: 1px solid grey;
            }

        </style>
    </head>
    <body>
        <h2>Testing <?php echo ($num = count($queries)) ?> quer<?php echo plural($num, "ies", "y") ?> <?php echo $times ?> time<?php echo plural($times) ?>.</h2>
        <h3>Database: <?php echo DB_DATABASE ?></h3>
        <?php
            $runs     = array();
            $averages = array();
            for($t = 0; $t < $times; $t++) {
                for($k = 0; $k < $num; $k++) {
                    $start = microtime(true);
                    $stmt  = $db->query($queries[$k]);
                    $time  = (microtime(true) - $start) * 1000;
                    @$averages[$k] += $time;
                    @$runs[$k][] = array(
                        'rows'   => $stmt->rowCount(),
                        'time'   => $time,
                        'errors' => $error = $stmt->errorInfo() ? $error[2] : ''
                    );
                }
            }
            asort($averages);
            $ranks = array();
            $rank = 1;
            foreach ($averages as $k => $average) $ranks[$k] = $rank++;
        ?>
        <table>
            <tbody>
                <tr>
                    <?php for($k = 0; $k < $num; $k++): ?>
                    <td style="font-size: 2em"><?php echo ordinalSuffix($ranks[$k]) ?></td>
                    <?php endfor ?>
                </tr>
                <tr>
                    <?php foreach ($queries as $key => $query): ?>
                    <td><pre><?php echo $query ?></pre></td>
                    <?php endforeach ?>
                </tr>
                <tr>
                    <?php
                        for($k = 0; $k < $num; $k++):
                            $runTimes = array();
                            foreach ($runs[$k] as $run) {
                                $runTimes[] = $run['time'];
                            }
                            sort($runTimes);
                    ?>
                    <td>                        
                        <table>
                            <tbody>
                                <tr>
                                    <th>Average</th>
                                    <td><?php echo $averages[$k] / $times ?></td>
                                </tr>
                                <tr>
                                    <th>Fastest</th>
                                    <td><?php echo $runTimes[0] ?></td>
                                </tr>
                                <tr>
                                    <th>Slowest</th>
                                    <td><?php echo $runTimes[$times - 1] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <thead>
                                <tr>
                                    <th>Run</th>
                                    <th>Rows Returned</th>
                                    <th>Milliseconds</th>
                                    <th>Errors</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($runs[$k] as $j => $run): ?>
                                <tr>
                                    <td><?php echo $j + 1 ?></td>
                                    <td><?php echo $run['rows'] ?></td>
                                    <td><?php echo $run['time'] ?></td>
                                    <td><?php echo $run['errors'] ?></td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </td>
                    <?php endfor ?>
                </tr>
            </tbody>
        </table>
    </body>
</html>
