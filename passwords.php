<?php

$characters = array(
    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    "_"
);
$specials = array('!','#','$','*','&', '.', '-', '?', '@', '^', '(', ')', '/', ',', '[', ']', '+', '%');

function formatNum($num) {
    $num = explode("E+", $num);
    if (count($num) != 2) return @number_format($num[0]) ?: $num[0];
    return "~" . round($num[0], 2) . " * 10<sup>" . $num[1] . "</sup>";
}

include "readablewords.php";

?><html>
    <head>
        <title>Password Generator</title>
        <style type="text/css">

            body {
                font-family: Arial;
            }

            hr {
                margin-top: 20px;
            }

            pre {
                white-space: pre-wrap;
                word-wrap: break-word;
            }

            .clear {
                clear: both;
            }

            .floatR {
                float: right;
            }

            .nowrap {
                white-space: nowrap;
            }

            .nowrap + .nowrap {
                margin-right: 64px;
                float: left;
            }

        </style>
    </head>
    <body>
        <?php

        $num = (int)(isset($_GET['num']) ? $_GET['num'] : 16);

        ?>
        <form method="get">
            <fieldset>
                <legend>Password Generator</legend>
                <label>Number of Characters: <input type="text" name="num" value="<?php echo $num ?>" /></label>
                <input type="submit" />
            </fieldset>
        </form>
        <pre><?php

        $total = count($characters) - 1;

        ?></pre>

        <span class="floatR"><?php echo implode("", $characters) ?></span>
    
        <h3>Double-Clickable (<?php echo formatNum(pow($total + 1, $num)) ?> combinations)</h3>

        <pre><?php

        for ($i = 0; $i < 150; $i++) {

            for ($j = 0; $j < $num; $j++) echo $characters[mt_rand(0, $total)];
            echo "\t";
        }

        $characters = array_merge($characters, $specials);
        $total = count($characters) - 1;

        ?></pre>
        
        <hr />

        <span class="floatR"><?php echo implode("", $characters) ?></span>

        <h3>Special Characters (<?php echo formatNum(pow($total + 1, $num)) ?> combinations)</h3>

        <pre><?php

        for ($i = 0; $i < 150; $i++) {

            echo '<span class="nowrap">';
            for ($j = 0; $j < $num; $j++) echo htmlentities($characters[mt_rand(0, $total)]);
            echo "</span>";
        }

        $multiplier = 100;
        $readables = array();
        $limit = 200;
        $biggest = 0;

        foreach ($words as $group) {

            $total = count($group);
            $multiplier *= $total;
            $total -= 1;

            for ($i = 0; $i < $limit + 1; $i++)
                @$readables[$i] .= ucfirst($group[mt_rand(0, $total)]);
        }

        foreach ($readables as &$readable) {

            $readable .= mt_rand(0, 9) . mt_rand(0, 9);

            if (($len = strlen($readable)) > $biggest) $biggest = $len;
        }

        array_pop($readables);

        ?></pre>

        <div class="clear"></div>
    
        <hr />

        <h3>Readable (<?php echo formatNum($multiplier) ?> combinations)</h3>

        <pre><?php

        foreach ($readables as $readable) echo str_pad($readable, $biggest + 1);

        ?></pre>
    </body>
</html>